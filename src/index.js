var semaine = [
  {
    jour: 'lundi',
    toDos: [['douche'], ['cours']],
  },
  {
    jour: 'mardi',
    toDos: [['douche'], ['cours'], ['synthese']],
  },
  {
    jour: 'mercredi',
    toDos: [['douche'], ['cours'], ['danse']],
  },
  {
    jour: 'jeudi',
    toDos: [['douche'], ['cours']],
  },
  {
    jour: 'vendredi',
    toDos: [['douche'], ['cours'], ['synthese']],
  },
  {
    jour: 'samedi',
    toDos: [['douche'], ['danse']],
  },
  {
    jour: 'dimanche',
    toDos: [['douche'], ['synthese']],
  },
];


for (const jour of semaine) {
  var myDiv = document.createElement('div');
  myDiv.className = 'largeur';
  var titre = document.createElement('h3');
  titre.innerText = jour.jour;
  myDiv.appendChild(titre);
  for (const toDo of jour.toDos) {
    if (toDo) {
      myDiv.innerHTML += `<input type="checkbox"/> <label for="${toDo[0]}">${toDo[0]}</label> <br/>`;
    }
  }
  document.body.appendChild(myDiv);
}

var checkBoxes = document.querySelectorAll('input[type=checkbox]');
for (const cb of checkBoxes) {
  console.log(cb);
}
// checkBoxes.forEach(element => {
//   console.dir(element);
//   element.addEventListener('mouseover', e => {
//     console.dir(e);
//     // e.nextElementSibling.backgroundColor = 'blue';
//   });
// });

// var btn = document.createElement('button');
// btn.innerText = 'save';
// document.body.appendChild(btn);
